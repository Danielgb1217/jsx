# Trabajo javascript 
_Ejercicios JSX_

## Integrantes grupos
1. Jhonathan Grisales
2. Daniel Andres Gonzales
##

### EJERCICIO 1

```html
   

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="style.css" media="screen, print, projection" />

</head>

<body >
    <div id="root">
        <!--Render code React-js-->
    </div>

    <!-- Cargar React. -->
    <!-- Nota: cuando se despliegue, reemplazar "development.js" con "production.min.js". -->
    <script src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
    <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
    <!-- Cargamos nuestro componente de React. -->
    <script type="text/babel">

        //EJERCICIO UNO 

        const datos = {

            tituloUNo: "Plato de la semana",
            tituloDos: "Berenjenas fritas",
            parrafoUno: "Comensales: 4 personas",
            parrafoDos: "Tiempo de reparación: 10 minutos",
            parrafoTres: "Tiempo de cocción: 12 minutos",
            parrafoCuatro: "Ingredientes:",
            parrafoCinco: "4 berenjenas.",
            parrafoSeis: "Sal",
            parrafoSiete: "Pimienta",
            parrafoOcho: "4 cucharadas de harina y aceite",
            parrafoNueve: "Preparación",
            parrafoDiez: "Lavar las berenjenas.",
            parrafoOnce: "Cortarlas en rodaias.",
            parrafoDoce: "Espolvorearlas con sal.",
            parrafoTrece: "Dejar que suelten el agua durante 30 minutos.",
            parrafoCatorce: "Enharizarlas, ponerlas a freir durante 5 minutos en aceite bien caliente.",

        }


        const elementos = (
            <div>
                <h1>{datos.tituloUNo}</h1>
                <h2>{datos.tituloDos}</h2>
                <p>{datos.parrafoUno}</p>
                <br />
                <p>{datos.parrafoDos}</p>
                <br />
                <p>{datos.parrafoTres}</p>
                <br />
                <p>{datos.parrafoCuatro}</p>
                <br />
                <a>{datos.parrafoCinco}</a>
                <p>{datos.parrafoSeis}</p>
                <p>{datos.parrafoSiete}</p>
                <p>{datos.parrafoOcho}</p>
                <br />
                <p>{datos.parrafoNueve}</p>
                <br />
                <p>{datos.parrafoDiez}</p>
                <p>{datos.parrafoOnce}</p>
                <p>{datos.parrafoDoce}</p>
                <p>{datos.parrafoTrece}</p>
                <p>{datos.parrafoCatorce}</p>

            </div>
        );



        // Renderiamos o pintamos
        ReactDOM.render(
            elementos,// estructura jsx para pintar
            document.getElementById('root')// elemento donde se pintara
        );

  

    </script>

</body>

</html>

```

![Alt text](./img/4.jpg?raw=true "Title")

### EJERCICIO 2

```html

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="style.css" media="screen, print, projection" />

</head>

<body>
    <div id="root">
        <!--Render code React-js-->
    </div>

    <!-- Cargar React. -->
    <!-- Nota: cuando se despliegue, reemplazar "development.js" con "production.min.js". -->
    <script src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
    <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
    <!-- Cargamos nuestro componente de React. -->
    <script type="text/babel">


        //EJERCICIO DOS

        const datosDos = {

            ejerDosTituloUNo: "Página inicial de Juan Tuesta",
            ejerDosParrafoDos: "Bienvenido a mi página personal. Soy un alumno de la Universidad de Deusto y esta es mi página inicial, con la lista de mis enlaces favoritos y otra información de interés",
            ejerDosSubtituloUno: "Enlaces favoritos:",
            ejerDosParrafoTres: "Internet",
            ejerDosParrafoCuatro: "Google",
            ejerDosParrafoCinco: "Aldea global",
            ejerDosParrafoSeis: "Manual de HTML",
            ejerDosParrafoSiete: "Juan Tuesta, Universidad de Deusto, marzo 2002.",
        }

        const elementos = (
            <div>
                <h1>{datosDos.ejerDosTituloUNo}</h1>
                <p>{datosDos.ejerDosParrafoDos}</p>
                <br />
                <h2>{datosDos.ejerDosSubtituloUno}</h2>

                <ol>
                    <li>{datosDos.ejerDosParrafoTres}</li>
                    <li>{datosDos.ejerDosParrafoCuatro}</li>
                    <li>{datosDos.ejerDosParrafoCinco}</li>
                    <li>{datosDos.ejerDosParrafoSeis}</li>
                </ol>
                <br />
                <i>{datosDos.ejerDosParrafoSiete}</i>

            </div>
        );

        // Renderiamos o pintamos
        ReactDOM.render(
            elementos,// estructura jsx para pintar
            document.getElementById('root')// elemento donde se pintara
        );

    </script>

</body>

</html>

```

![Alt text](./img/5.jpg?raw=true "Title")




### EJERCICIO 3

```html

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="style.css" media="screen, print, projection" />

</head>

<body>
    <div id="root">
        <!--Render code React-js-->
    </div>

    <!-- Cargar React. -->
    <!-- Nota: cuando se despliegue, reemplazar "development.js" con "production.min.js". -->
    <script src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
    <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
    <!-- Cargamos nuestro componente de React. -->

    <script type="text/babel">

        //////////////////////////////////////////////////////////////////


        //EJERCICIO TRES


        const datos = {

            TituloUNo: "Página inicial de Juan Tuesta",
            ParrafoDos: "Bienvenido a mi página personal. Soy un alumno de la Universidad de Deusto y esta es mi página inicial, con la lista de mis enlaces favoritos y otra información de interés",
            SubtituloUno: "Enlaces favoritos:",
            ParrafoTres: "Internet",
            ParrafoCuatro: "Google",
            ParrafoCinco: "Aldea global",
            ParrafoSeis: "Manual de HTML",
            ParrafoSiete: "Juan Tuesta, Universidad de Deusto, marzo 2002.",

        }

        const elemento = (
            <div>
                <h1>{datos.TituloUNo}</h1>
                <p>{datos.ParrafoDos}</p>
                <br />
                <h2>{datos.SubtituloUno}</h2>

                <ol>
                    <li>{datos.ParrafoTres}</li>
                    <li><a href="./index.html" >{datos.ParrafoCuatro}</a></li>
                    <li>{datos.ParrafoCinco}</li>
                    <li><a href="./index.html">{datos.ParrafoSeis}</a></li>
                </ol>
                <br />
                <i>{datos.ParrafoSiete}</i>

            </div>
        );
        // Renderiamos o pintamos
        ReactDOM.render(
            elemento,// estructura jsx para pintar
            document.getElementById('root')// elemento donde se pintara
        );



    </script>

</body>

</html>

```


![Alt text](./img/6.jpg?raw=true "Title")




### EJERCICIO 4

```html
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="style.css" media="screen, print, projection" />

</head>

<body>
    <div id="root">
        <!--Render code React-js-->
    </div>


    <!-- Cargar React. -->
    <!-- Nota: cuando se despliegue, reemplazar "development.js" con "production.min.js". -->
    <script src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
    <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
    <!-- Cargamos nuestro componente de React. -->

    <script type="text/babel">

        //////////////////////////////////////////////////////////////////


        //EJERCICIO CUATRO

        const datos = {

            TituloUNo: "Página inicial de Juan Tuesta",
            ParrafoDos: "Bienvenido a mi página personal. Soy un alumno de la Universidad de Deusto y esta es mi página inicial, con la lista de mis enlaces favoritos y otra información de interés",
            SubtituloUno: "Enlaces favoritos:",
            ParrafoUno: "Páginas personales",
            ParrafoTres: "Charkes F. Golfarb",
            ParrafoCuatro: "Lou Burnard",
            ParrafoCinco: "Tim Berners-Lee",
            ParrafoSeis: "Páginas de referencia",
            ParrafoSiete: "Portales",
            ParrafoOcho: "Medios de comunicación",
            ParrafoNueve: "Juan Tuesta, Universidad de Deusto, marzo 2002"

        }


        const elemento = (
            <div>
                <h1>{datos.TituloUNo}</h1>
                <p>{datos.ParrafoDos}</p>
                <br />
                <h2>{datos.SubtituloUno}</h2>

                <ol>
                    <li>{datos.ParrafoUno}</li>

                    <ul>
                        <li>{datos.ParrafoTres}</li>
                        <li>{datos.ParrafoCuatro}</li>
                        <li>{datos.ParrafoCinco}</li>
                    </ul>

                    <li>{datos.ParrafoSeis}</li>
                    <li>{datos.ParrafoSiete}</li>
                    <li>{datos.ParrafoOcho}</li>

                </ol>
                <br />
                <i>{datos.ParrafoNueve}</i>

            </div>
        );
        // Renderiamos o pintamos
        ReactDOM.render(
            elemento,// estructura jsx para pintar
            document.getElementById('root')// elemento donde se pintara
        );


    </script>

</body>

</html>


```

![Alt text](./img/7.jpg?raw=true "Title")



### EJERCICIO 5

```html

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="style.css" media="screen, print, projection" />

</head>

<body>
    <div id="root">
        <!--Render code React-js-->
    </div>


    <!-- Cargar React. -->
    <!-- Nota: cuando se despliegue, reemplazar "development.js" con "production.min.js". -->
    <script src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
    <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
    <!-- Cargamos nuestro componente de React. -->

    <script type="text/babel">

        //////////////////////////////////////////////////////////////////


        //EJERCICIO CINCO

        const datos = {

            TituloUNo: "Página inicial de Juan Tuesta",
            ParrafoDos: "Bienvenido a mi página personal. Soy un alumno de la Universidad de Deusto y esta es mi página inicial, con la lista de mis enlaces favoritos y otra información de interés",

            SubtituloUno: "Enlaces favoritos:",
            ParrafoUno: "Páginas personales",
            ParrafoTres: "Páginas referencia",
            ParrafoCuatro: "Portales",
            ParrafoCinco: "Publicaciones",

            TituloDos: "Páginas personales",
            itemUno: "Documentalistas",
            itemDos: "Charkes F. Golfarb",
            itemTres: "Lou Burnard",
            itemCuatro: "Tim Berners-Lee",
            itemCinco: "Historiadores",
            itemSeis: "Lingüística",
            itemSiete: "Traductores",

            tituloTres: "Páginas referencia",
            mItemUNo: "Bibliotecas universitarias",
            mItemDos: "Centros de documentación",
            mItemTres: "Museos de arte contemporáneo",
            mItemCuatro: "Medios de comunicación",

            tituloCuatro: "Portales",
            menuItemUno: "Inicia",
            menuItemDos: "Telépolis",
            menuItemTres: "Ciudades",
            menuItemCuatro: "Terra",

            tituloCinco: "Publicaciones",
            itemUnoMenu: "Ciberp@is",
            itemDosMenu: "Diario Tecnologías de la Información",
            itemTresMenu: "The Standard",
            itemCuatroMenu: "Infoworld",
            itemCincoMenu: "Wired",

            pieDePagina: "Juan Tuesta, Universidad de Deusto, marzo 2002",
            linea: "____________________________________________________________________________________________________________"

        }


        const elemento = (
            <div>
                <h1>{datos.TituloUNo}</h1>
                <p>{datos.ParrafoDos}</p>
                <br />
                <h2>{datos.SubtituloUno}</h2>

                <ol>
                    <li><a href="./index.html">{datos.ParrafoUno}</a></li>
                    <li>{datos.ParrafoTres}</li>
                    <li>{datos.ParrafoCuatro}</li>
                    <li>{datos.ParrafoCinco}</li>
                </ol>

                <p>{datos.linea}</p>

                <h2>{datos.TituloDos}</h2>

                <ol>
                    <li>{datos.itemUno}</li>
                    <ul>
                        <li>{datos.itemDos}</li>
                        <li>{datos.itemTres}</li>
                        <li>{datos.itemCuatro}</li>
                    </ul>

                    <li>{datos.itemCinco}</li>
                    <li>{datos.itemSeis}</li>
                    <li>{datos.itemSiete}</li>
                </ol>

                <p>{datos.linea}</p>

                <h2>{datos.tituloTres}</h2>

                <ol>
                    <li>{datos.mItemUNo}</li>
                    <li>{datos.mItemDos}</li>
                    <li>{datos.mItemTres}</li>
                    <li>{datos.mItemCuatro}</li>
                </ol>

                <p>{datos.linea}</p>

                <h2>{datos.tituloCuatro}</h2>

                <ol>
                    <li>{datos.menuItemUno}</li>
                    <li>{datos.menuItemDos}</li>
                    <li>{datos.menuItemTres}</li>
                    <li>{datos.menuItemCuatro}</li>
                </ol>

                <p>{datos.linea}</p>

                <h2>{datos.tituloCinco}</h2>

                <ol>
                    <li>{datos.itemUnoMenu}</li>
                    <li>{datos.itemDosMenu}</li>
                    <li>{datos.itemTresMenu}</li>
                    <li>{datos.itemCuatroMenu}</li>
                    <li>{datos.itemCincoMenu}</li>
                </ol>

                <p>{datos.linea}</p>
                <br />
                <i>{datos.pieDePagina}</i>

            </div>
        );
        // Renderiamos o pintamos
        ReactDOM.render(
            elemento,// estructura jsx para pintar
            document.getElementById('root')// elemento donde se pintara
        );


    </script>

</body>

</html>


```

![Alt text](./img/8.jpg?raw=true "Title")



### EJERCICIO 6

```html

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="style.css" media="screen, print, projection" />

</head>

<body>
    <div id="root">
        <!--Render code React-js-->
    </div>


    <!-- Cargar React. -->
    <!-- Nota: cuando se despliegue, reemplazar "development.js" con "production.min.js". -->
    <script src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
    <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
    <!-- Cargamos nuestro componente de React. -->
    <script type="text/babel">


        //EJERCICIO SEIS

        const datosDos = {

            tituloUNo: "El Noticiero Next University",
            subtituloUNo: "Programas de tecnología",
            subtituloDos: "Desarrollador Web",

            parrafoUNo: "Next University",
            parrafoDos: "lanza su programa de formación",
            parrafoTres: "Desarrollador Web",
            parrafoCuatro: "con la finalidad de capacitar a personas en las tendencias actuales sobre tecnologias, en el caso de desarrollo web, se basa temas relacionados a la implementación de Front-End con tecnologias y Frameworks, tales como:",
            parrafoCinco: "HTML5, CSS, JavaScript, Boostrap, entre otros.",

            subtituloTres: "Android",
            mparrafoUno:"En",
            mparrafoDos:"Next University",
            mparrafoTres:"te ofrecemos el programa",
            mparrafoCuatro:"Android",
            mparrafoCinco:"que te da las herramientas necesarias para diseñar e implementar aplicaciones Android para dispositivos móviles, partiendo de un conocimiento básico de ",
            mparrafoSeis:"Java,",
            mparrafoSiete:"utilizando el entorno de desarrollo",
            mparrafoOcho:"Anaroid Studio.",
            mparrafoNueve:"Aprenderás los aspectos fundamentales para crear aplicaciones Android interactivas, dinámicas y exitosas utilizando técnicas para el manejo de los recursos, datos, segundos planos, localización, sensores, animaciones, gráficos, multimedia y monetizacio",


            subtituloCuatro:"Programa de Negocio",
            subtituloCinco: "Mercadeo Digital",
            menuParrafoUNo:"Next University",
            menuParrafoDos:"coloca a tu disposición de programa de",
            menuParrafoTres:"Mercadeo Digital",
            menuParrafoCuatro: "Sabias que... Mearcadeo Digital o Mercadeo Marketing es mucho más que hacer y publicar anuncios. Se trata del consumidor y la estrategia de negocio. Estos son los puntos de partida para que cualquier acción de mercadeo digital ",
            menuParrafoCinco: "sea exitoso y optomice la inversión.",
            menuParrafoSeis:"Este certificado, es un programa completo y práctico, que permite entender al consumidor, la industria, la competencia y los retos del negocio propio; para crear e implementar estrategias a la medida, en diferentes plataformas y medios digitales. Por supuesto, monitoreando los resultados para tomar decisiones en tiempo real.",



        }


        const elementos = (
            <div>
                <h1>{datosDos.tituloUNo}</h1>
                <h2>{datosDos.subtituloUNo}</h2>
                <h2>{datosDos.subtituloDos}</h2>
                <p><i>{datosDos.parrafoUNo}</i> {datosDos.parrafoDos} <b>"{datosDos.parrafoTres}"</b> {datosDos.parrafoCuatro} <i>{datosDos.parrafoCinco}</i></p>

                <h2>{datosDos.subtituloTres}</h2>
                <p>{datosDos.mparrafoUno} <b>{datosDos.mparrafoDos}</b> {datosDos.mparrafoTres} <b>{datosDos.mparrafoCuatro}</b>
                    
                {datosDos.mparrafoCinco} <i>{datosDos.mparrafoSeis}</i> {datosDos.mparrafoSiete} <i>{datosDos.mparrafoOcho}</i> {datosDos.mparrafoNueve} </p>
                <br />
                <h1>{datosDos.subtituloCuatro}</h1>
                <h3>{datosDos.subtituloCinco}</h3>
                <p> <b><i>{datosDos.menuParrafoUNo}</i></b> {datosDos.menuParrafoDos} <b>"{datosDos.menuParrafoTres}"</b> {datosDos.menuParrafoCuatro} 
                    <i>{datosDos.menuParrafoCinco}</i> {datosDos.menuParrafoSeis}</p>



            </div>
        );

        // Renderiamos o pintamos
        ReactDOM.render(
            elementos,// estructura jsx para pintar
            document.getElementById('root')// elemento donde se pintara
        );

    </script>

</body>

</html>

```


![Alt text](./img/9.jpg?raw=true "Title")



### EJERCICIO 7

```html

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="style.css" media="screen, print, projection" />

</head>

<body>
    <div id="root">
        <!--Render code React-js-->
    </div>



    <!-- Cargar React. -->
    <!-- Nota: cuando se despliegue, reemplazar "development.js" con "production.min.js". -->
    <script src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
    <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
    <!-- Cargamos nuestro componente de React. -->
    <script type="text/babel">


        //EJERCICIO SIETE

        const datos = {

            tituloUNo: "Destinos Turísticos",
            tituloDos: "América",

            tituloMenuUno: "Argentina",
            itemMenuUno: "Buenos Aires",
            itemMenuDos: "Boriloche",
            tituloMenuDos: "Brasil",
            itemMenuTres: "Rio de Janeiro",
            itemMenuCuatro: "Brasilia",
            tituloMenuTres: "Chile",
            itemMenuCinco: "Santiago",
            itemMenuSeis: "Valparaiso",
            tituloMenuCuatro:"Colombia",
            itemMenuSiete:"Bogotá",
            itemMenuOcho:"Cartagena de Indias",
            tituloMenuCinco: "Estados unidos",
            itemMenuNueve:"New York",
            itemMenuDiez:"San Francisco",
            tituloMenuSeis:"Mexico",
            itemMenuOnce:"Cancún",
            itemMenuDoce:"Acapulco",
            tituloMenuSiete:"Perú",
            itemMenuTrece:"Lima",
            itemMenuCatorce:"Cuzco",
            tituloMenuOcho:"Venezuela",
            itemMenuQuince:"Margarita",
            itemMenudieciseis:"Mérida",
           







        }



        const elementos = (
            <div>

                <h1>{datos.tituloUNo}</h1>
                <h2>{datos.tituloDos}</h2>
                <ol>
                    <li><b>{datos.tituloMenuUno}</b></li>
                    <i><ol><li>{datos.itemMenuUno}</li> <li>{datos.itemMenuDos}</li></ol></i>

                    <li><b>{datos.tituloMenuDos}</b></li>
                    <i><ol><li>{datos.itemMenuTres}</li> <li>{datos.itemMenuCuatro}</li></ol></i>

                    <li><b>{datos.tituloMenuTres}</b></li>
                    <i><ol><li>{datos.itemMenuCinco}</li> <li>{datos.itemMenuSeis}</li></ol></i>

                    <li><b>{datos.tituloMenuCuatro}</b></li>
                    <i><ol><li>{datos.itemMenuSiete}</li> <li>{datos.itemMenuOcho}</li></ol></i>

                    <li><b>{datos.tituloMenuCinco}</b></li>
                    <i><ol><li>{datos.itemMenuNueve}</li> <li>{datos.itemMenuDiez}</li></ol></i>

                    <li><b>{datos.tituloMenuSeis}</b></li>
                    <i><ol><li>{datos.itemMenuOnce}</li> <li>{datos.itemMenuDoce}</li></ol></i>

                    <li><b>{datos.tituloMenuSiete}</b></li>
                    <i><ol><li>{datos.itemMenuTrece}</li> <li>{datos.itemMenuCatorce}</li></ol></i>

                    <li><b>{datos.tituloMenuOcho}</b></li>
                    <i><ol><li>{datos.itemMenuQuince}</li> <li>{datos.itemMenudieciseis}</li></ol></i>

                </ol>


            </div>
        );



        // Renderiamos o pintamos
        ReactDOM.render(
            elementos,// estructura jsx para pintar
            document.getElementById('root')// elemento donde se pintara
        );



    </script>

</body>

</html>

```

![Alt text](./img/10.jpg?raw=true "Title")



### EJERCICIO 8

```html

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="style.css" media="screen, print, projection" />

</head>

<body>
    <div id="root">
        <!--Render code React-js-->
    </div>



    <!-- Cargar React. -->
    <!-- Nota: cuando se despliegue, reemplazar "development.js" con "production.min.js". -->
    <script src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
    <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>

    <!-- Cargamos nuestro componente de React. -->
    <script type="text/babel">


        //EJERCICIO OCHO

        const datos = {

            tituloUNo: "Tu concierto",
            parrafoUno: "Beinvenidos a este Blog de conciertos...",
            imagen : "./img/1.jpg",
            imagenDos : "./img/2.jpg",

        }

        

        const elementos = (

            <div>

                <h1> {<img src={datos.imagen} />} {datos.tituloUNo}</h1>
                <p>{datos.parrafoUno}</p>
                <br/>
                {<img src={datos.imagenDos} />}



            </div>
        );



        // Renderiamos o pintamos
        ReactDOM.render(
            elementos,// estructura jsx para pintar
            document.getElementById('root')// elemento donde se pintara
        );



    </script>

</body>

</html>



```

![Alt text](./img/11.jpg?raw=true "Title")


### EJERCICIO 9

```html

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="style.css" media="screen, print, projection" />

</head>

<body>
    <div id="root">
        <!--Render code React-js-->
    </div>


    <!-- Cargar React. -->
    <!-- Nota: cuando se despliegue, reemplazar "development.js" con "production.min.js". -->
    <script src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
    <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
    <!-- Cargamos nuestro componente de React. -->
    <script type="text/babel">


        //EJERCICIO NUEVE

        const datos = {

            titulo: "Cartelera de Vuelos - Aeropuerto el mirador",
            subtitulo: "Llegadas",

            item: "Aerolinea",
            itemDos: "Nro. Vuelo",
            itemTres: "Estatus",
            itemCuatro: "Hora estimada",
            itemCinco: "puerta",

            itemSeis: "AIRLAN",
            itemSiete: "355",
            itemOcho: "ATERRIZO",
            itemNueve: "2:45PM",
            itemDiez: "3",

            item1: "VIAJAR",
            item2: "556",
            item3: "RETARDADO",
            item4: "2:35PM",
            item5: "6",


            unoItem: "AEROVUELO",
            dosItem: "1234",
            tresItem: "EN VUELO",
            cuatroItem: "3:45PM",
            cincoItem: "8",

            subtituloDos: "Salidas",

            mItemUno:"EMBARCANDO",
            mItemDos:"2:15PM",
            mItemTres:"1",
           



        }


        const elementos = (
            <div>

                <h1>{datos.titulo}</h1>
                <h2>{datos.subtitulo}</h2>

                <table border="1" cellpadding="7" cellspacing="0">
                    <tr>
                        <th>{datos.item}</th>
                        <th>{datos.itemDos}</th>
                        <th>{datos.itemTres}</th>
                        <th>{datos.itemCuatro}</th>
                        <th>{datos.itemCinco}</th>
                    </tr>

                    <tr>
                        <td>{datos.itemSeis}</td>
                        <td>{datos.itemSiete}</td>
                        <td>{datos.itemOcho}</td>
                        <td>{datos.itemNueve}</td>
                        <td>{datos.itemDiez}</td>

                    </tr>

                    <tr>
                        <td>{datos.item1}</td>
                        <td>{datos.item2}</td>
                        <td>{datos.item3}</td>
                        <td>{datos.item4}</td>
                        <td>{datos.item5}</td>

                    </tr>

                    <tr>
                        <td>{datos.unoItem}</td>
                        <td>{datos.dosItem}</td>
                        <td>{datos.tresItem}</td>
                        <td>{datos.cuatroItem}</td>
                        <td>{datos.cincoItem}</td>

                    </tr>

                </table>

                <h2>{datos.subtituloDos}</h2>


                <table border="1" cellpadding="7" cellspacing="0">
                    <tr>
                        <th>{datos.item}</th>
                        <th>{datos.itemDos}</th>
                        <th>{datos.itemTres}</th>
                        <th>{datos.itemCuatro}</th>
                        <th>{datos.itemCinco}</th>
                    </tr>

                    <tr>
                        <td>{datos.itemSeis}</td>
                        <td>{datos.itemSiete}</td>
                        <td>{datos.mItemUno}</td>
                        <td>{datos.mItemDos}</td>
                        <td>{datos.mItemTres}</td>

                    </tr>

                   

                </table>



            </div>
        );

        // Renderiamos o pintamos
        ReactDOM.render(
            elementos,// estructura jsx para pintar
            document.getElementById('root')// elemento donde se pintara
        );

    </script>

</body>

</html>


```


![Alt text](./img/12.jpg?raw=true "Title")




### EJERCICIO 10

```html


<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="style.css" media="screen, print, projection" />

</head>

<body>
    <div id="root">
        <!--Render code React-js-->
    </div>



    <!-- Cargar React. -->
    <!-- Nota: cuando se despliegue, reemplazar "development.js" con "production.min.js". -->
    <script src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
    <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
    <!-- Cargamos nuestro componente de React. -->
    <script type="text/babel">


        //EJERCICIO UNO 

        const datos = {

            tituloUNo: "Recetas.com",
            imagen: "./img/3.jpg",
            menuUmo: "Recetas Principales   ",
            barra: "|",
            menuDos: "  Postres latinos",

            parrafoUno: "Bienvenidos a nuestro sitio Web sobre gastronomia latina, te invitamos a preparar nuestras recetas",

            tituloDos: "Nuevas Recetas",
            parrafoDos: "**Arroz Criollo*",

            parrafoTres: "Tiempo de preparación: ",
            parrafoCuatro: "45 minutos",
            parrafoCinco: "Numero de porciones",
            parrafoSeis: "4",

            parrafoSiete: "Ingredientes",
            item1: "Salsa de tomate",
            item2: "2 cucharadas de aceite 14gr",
            item3: "1 cebolla larga picada 45gr",
            item4: "2 dientes de ajo finamente picado 3gr",
            item5: "1 pimentón rojo picado en cuadritos 60gr",
            item6: "1 taza de arroz blanco 225gr",
            item7: "1 taza de maíz desgranado 79gr",
            item8: "150gr de pechuga de pollo cocinado y desmechado",
            item9: "Sal al gusto",

            parraOcho: "Preparación",
            item_1: "Calentar en una olla el aceite, sofreir la cebolla junto con el ajo, el pimentin por 3 minutos",
            item_2: "Adicionar el arroz y continua sofriendo para que se dore.",
            item_3: "Agregar el pollo, el maiz desgranado y las verduras.",
            item_4: "Mezclar todo, rectificar sal y dejar cocinar hasta que seque.",
            item_5: "Bajar el fuego, tapar la olla y continuar la cocción por 25 minutos más.",


            pieDePagina: "Esta rica receta fue proporcionada por Maria Paula.",

        }

        const elementos = (
            <div>

                <h1> {<img src={datos.imagen} />} {datos.tituloUNo}</h1>

                <h3><a href="">{datos.menuUmo}</a>  {datos.barra}  <a href=""> {datos.menuDos}</a></h3>
                <br />

                <p>{datos.parrafoUno}</p>

                <h1>{datos.tituloDos}</h1>

                <h2>{datos.parrafoDos}</h2>

                <p>{datos.parrafoTres} <b>{datos.parrafoCuatro}</b> {datos.parrafoCinco} <b>{datos.parrafoSeis}</b> </p>

                <h2>{datos.parrafoSiete}</h2>

                <ul>

                    <li><b>{datos.item1}</b></li>
                    <li><b>{datos.item2}</b></li>
                    <li><b>{datos.item3}</b></li>
                    <li><b>{datos.item4}</b></li>
                    <li><b>{datos.item5}</b></li>
                    <li><b>{datos.item6}</b></li>
                    <li><b>{datos.item7}</b></li>
                    <li><b>{datos.item8}</b></li>
                    <li><b>{datos.item9}</b></li>

                </ul>

                <h2>{datos.parraOcho}</h2>

                <ol>

                    <li>{datos.item_1}</li>
                    <li>{datos.item_2}</li>
                    <li>{datos.item_3}</li>
                    <li>{datos.item_4}</li>
                    <li>{datos.item_5}</li>

                </ol>

                <p>{datos.pieDePagina}</p>


            </div>

        );



        // Renderiamos o pintamos
        ReactDOM.render(
            elementos,// estructura jsx para pintar
            document.getElementById('root')// elemento donde se pintara
        );



    </script>

</body>

</html>

```

![Alt text](./img/13.jpg?raw=true "Title")



### EJERCICIO 11

```html

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="style.css" media="screen, print, projection" />

</head>

<body>
    <div id="root">
        <!--Render code React-js-->
    </div>


    <!-- Cargar React. -->
    <!-- Nota: cuando se despliegue, reemplazar "development.js" con "production.min.js". -->
    <script src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
    <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>
    <!-- Cargamos nuestro componente de React. -->
    <script type="text/babel">


        //EJERCICIO DOS

        const datos = {

            tituloUNo: "Registro en nuestra tienda",
            nombre: "Nombre del usuario:",
            email: "Email:",
            edad: "Edad",
            genero: "Género",
            recomendado: "Recomendado por:",
            comentarios: "Comentarios:",
            acepto:"Acepto terminos y condiciones:"

        }


        const elementos = (
            <div>
                <h1>{datos.tituloUNo}</h1>
                <p>{datos.nombre}</p>

                <input type="text" placeholder="Ej: luis" ></input>

                <br />
                <br />

                <p>{datos.email}</p>
                <input type="text" placeholder="Ej: luis@gmail.com" ></input>

                <br />
                <br />

                <p>{datos.edad}</p>
                <input type="text" placeholder="Ej: 21" ></input>

                <br />
                <br />

                <p>{datos.genero}</p>

                <input type="radio" value="Masculino" name="Genero" /> Masculino
                <br />
                <input type="radio" value="Femenino" name="Genero" /> Femenino

                <br />
                <br />
                <p>{datos.recomendado}</p>


                <select value={datos.recomendado}>
                    <option value="A">Google</option>
                    <option value="B">Amazon</option>
                    <option value="C">Facebook</option>
                </select>


                <br />
                <br />
                <p>{datos.comentarios}</p>

                <textarea rows={4} cols={50} type="textarea"
                    name="textValue"
                />

                <br />
                <br />
                <p>{datos.acepto}</p>
                <input type="checkbox" />
                <br />
                <br />

                <input type="submit" value="Registrar" />


            </div>
        );

        // Renderiamos o pintamos
        ReactDOM.render(
            elementos,// estructura jsx para pintar
            document.getElementById('root')// elemento donde se pintara
        );

    </script>

</body>

</html>

```

![Alt text](./img/14.jpg?raw=true "Title")



### EJERCICIO 12

```html

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="style.css" media="screen, print, projection" />

</head>

<body>
    <div id="root">
        <!--Render code React-js-->
    </div>


    <!-- Cargar React. -->
    <!-- Nota: cuando se despliegue, reemplazar "development.js" con "production.min.js". -->
    <script src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
    <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>

    <!-- Cargamos nuestro componente de React. -->
    <script type="text/babel">


        //EJERCICIO DOS

        const dato = {

            titulo: "Título en encabezado",
            descrip: "Descripción del audio",
            audio: "./img/audio.mp3",
            drechos: "Derechos Reservados (Pie)",

        }


        const elementos = (
            <div>

                <h1>{dato.titulo}</h1>
                <p>{dato.descrip}</p>
                <br />

                <audio controlsList="nodownload" controls>

                    <source src="https://file-examples-com.github.io/uploads/2017/11/file_example_MP3_700KB.mp3" type="audio/mpeg" />
                </audio>
                <br />
                <br />

                <p>{dato.drechos}</p>


            </div>
        );

        // Renderiamos o pintamos
        ReactDOM.render(
            elementos,// estructura jsx para pintar
            document.getElementById('root')// elemento donde se pintara
        );

    </script>

</body>

</html>

```

![Alt text](./img/15.jpg?raw=true "Title")


### EJERCICIO 13

```html

<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <title></title>
    <link rel="stylesheet" type="text/css" href="style.css" media="screen, print, projection" />

</head>

<body>
    <div id="root">
        <!--Render code React-js-->
    </div>


    <!-- Cargar React. -->
    <!-- Nota: cuando se despliegue, reemplazar "development.js" con "production.min.js". -->
    <script src="https://unpkg.com/react@16/umd/react.development.js"></script>
    <script src="https://unpkg.com/react-dom@16/umd/react-dom.development.js"></script>
    <script src="https://unpkg.com/babel-standalone@6/babel.min.js"></script>

    <!-- Cargamos nuestro componente de React. -->
    <script type="text/babel">


        //EJERCICIO DOS

        const dato = {

            titulo: "Título en encabezado",
            descrip: "Este es un párrafo",
            video: "./img/video.mp4",
            drechos: "Derechos Reservados (Pie)",

        }


        const elementos = (
            <div>

                <h1>{dato.titulo}</h1>
                <p>{dato.descrip}</p>
               

                <video width="750" height="500" controls >
                    <source src={dato.video} type="video/mp4" />
                </video>
                <br />
                <br />

                <p>{dato.drechos}</p>


            </div>
        );

        // Renderiamos o pintamos
        ReactDOM.render(
            elementos,// estructura jsx para pintar
            document.getElementById('root')// elemento donde se pintara
        );

    </script>

</body>

</html>

```

![Alt text](./img/16.jpg?raw=true "Title")














